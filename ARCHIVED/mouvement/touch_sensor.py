#!/usr/bin/env python3
from ev3dev.brickpi3 import *

from ev3dev2.motor import LargeMotor, OUTPUT_A, OUTPUT_B, SpeedPercent, MoveTank
from ev3dev2.sensor import INPUT_2
from ev3dev2.sensor.lego import TouchSensor
from ev3dev2.led import Leds

ts = TouchSensor(INPUT_2)
m = LargeMotor(OUTPUT_A)

while True:
    if ts1.value() == 0:
      m.stop(stop_action="brake")
    else:
      m.run_forever(speed_sp=180)   # equivalent to power=20 in EV3-G