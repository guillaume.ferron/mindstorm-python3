#/usr/bin/python3

from mouvement.mouvementdebase import MouvementDeBase

class Telecommande:

    def __init(self):
        #self.api = MouvementDeBase()
        pass

    def help(self):
        print("Command : i[1-x]:up j[1-x]:left l[1-x]:right k[1-x]:down")
        print("Example : i3,l2,j2,k3")

    def console(self):
        command = ''
        print("input command (or exit) : ")
        while command != 'exit':
            commands = input().split(',');
            for i in commands:
                command = list(i)
                direction = command.pop(0)
                duration = 0
                print("hello")
                try:
                    for n in command:
                        duration = duration+n
                    if duration != "":
                        duration = float(duration)
                    else:
                        duration = 3
                    if direction == 'j':
                        MouvementDeBase.Gauche(duration)
                    elif direction == 'l':
                        MouvementDeBase.Droite(duration);
                    elif direction == 'i':
                        MouvementDeBase.Avancer(duration)
                    elif direction == 'k':
                        MouvementDeBase.Reculer(duration)
                    else:
                        print("Command Error")
                        help()
                except:
                    print("direction "+direction)
                    print("duration "+str(duration))
                    print("Command Error")


