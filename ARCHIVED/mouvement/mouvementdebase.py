#!/usr/bin/python3

from ev3dev2.motor import LargeMotor, OUTPUT_A, OUTPUT_B, SpeedPercent, MoveTank

class MouvementDeBase:
    def __init__(self):
        pass

    def Avancer(self, duration=3):
        tank_drive = MoveTank(OUTPUT_A, OUTPUT_B)
        tank_drive.on_for_rotations(SpeedPercent(100), SpeedPercent(100), duration)

    def Reculer(self, duration=3):
        tank_drive = MoveTank(OUTPUT_A, OUTPUT_B)
        tank_drive.on_for_rotations(SpeedPercent(-100), SpeedPercent(-100), duration)

    def Droite(self, duration=3):
        m = LargeMotor(OUTPUT_B)
        m.on_for_rotations(SpeedPercent(90), duration)
    
    def Gauche(self, duration=3):
        m = LargeMotor(OUTPUT_A)
        m.on_for_rotations(SpeedPercent(90), duration)

