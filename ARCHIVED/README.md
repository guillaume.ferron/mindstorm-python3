# Mindstorm-python3

Programmation d'un robot Lego Mindstorm en Python3 de manière à ce qu'il se dirige vers des objets qu'il a préalablement identifié dans son environnement.

Suivi du projet : https://trello.com/b/d7IZ0nGa/mindstorm-project