from ev3dev.ev3 import *
from smbus import SMBus

# get a Port object to control the input port
in1 = LegoPort(address=INPUT_1)
# force the port into i2c-other mode so that the default driver is not automatically loaded
in1.mode = 'other-i2c'

# might need a short delay here (e.g. time.sleep(0.5)) to wait for the I2C port to be setup
# note: this delay should only be needed in ev3dev-jessie (4.4.x kernel), not ev3dev-stretch (4.9.x kernel)

bus = SMBus(3)  # bus number is input port number + 2
I2C_ADDRESS = 0x01  # the default I2C address of the sensor

while True:
    # read the number of object detected
    num = bus.read_byte_data(I2C_ADDRESS, 0x42)
    if num:
        x1, y1, x2, y2 = bus.read_i2c_block_data(I2C_ADDRESS, 0x44, 4)
