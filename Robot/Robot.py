class Robot:

    """

    **Spécifications**

    Cette classe est le coeur même du robot que nous allons construire. Elle :

    - Doit posséder une interface graphique
        - De contrôle
        - De configuration

    - L'interface doit être accessible:
        - Par le web
        - Par le terminal

    - Doit controler des éléments du kit LegoMindstorm
        - Tous les moteurs
        - Tous les capteurs

    - Doit tenir sur une carte micro sd

    - Doit reconnaitre des fruits
        - Banane
        - Pomme

    - Doit se diriger vers des fruits

    - Doit pouvoir implémenter des modules tiers
        - Doit incorporer un module de reconnaissance de caractère (cf. imane)

    - Doit organiser les exceptions et les erreurs

    - Doit être testable
        - Doit passer PEP8

    """

    debug = True;

    def TEST(self):
        """
        Automatise des pans de test
        """

        pass

    def ERRORLISTENER(self):
        """
        Reçoit toutes les erreurs et exceptions
        """
        pass

    def CODELENGTH(self):
        """
        Affiche à chaque exécution la place que prend le code sur le disque
        """
        # Algorithme en 4 étapes
        # - remonte à la source du projet
        # - descend récursivement dans tous les dossiers
        # - additionne la taille de tous les fichiers
        # - retourne un résultat en Megaoctets

        # Pour décider à quelle arborescence commence le projet
        # nous pouvons arbitrairement statuer que le fichier courant
        # est l'origine du projet et le point le plus haut dans la
        # hierarchie des dépendances (il dépend de tout).

        # À  partir de là, nous importons la librairie (standard) os
        # afin d'obtenir le chemin du dossier parent au fichier courant.


        import os;
        root = os.getcwd();


        # Une fois en possession de ce chemin, nous allons produire la liste de tous
        # les chemins menants vers des fichiers présents dans l'arborescence du repertoire.


        tree = [ os.path.abspath(a+"/"+d) for a, b, c in os.walk(root) for d in c ]


        # Reste à visiter chaque chemin afin d'additionner la taille en Octet des
        # fichiers trouvés.


        c = 0;
        for a in tree:
            c += os.path.getsize(a)


        # Enfin nous retournons le résultat en Megaoctets


        return str(c/1000)+" Mo";



    """
    * Package ModuleSystem

    """

    def EXECMODULE(self):
        """
        Passe le thread courant à un module tiers
        """
        pass

    def IMPORT(self):
        """
        Valide et importe un module tiers
        """
        pass

    """
    * Package Movement

    """

    def APPROACH(self):
        """
        Se déplace dans le but de rejoindre une cible
        """
        pass

    """
    * Package ImageDetection

    """

    def IMAGERECON(self):
        """
        Reconnaissance d'image basée sur Google
        """
        pass


    """
    * Package SSHServer

    """

    def CLI(self):
        """
        Affiche des messages dans le terminal
        """

    """
    * Package WebServer

    """

    def X(self):
        """
        Affiche des vues html
        """
        pass

    def HTTP(self):
        """
        Lance un serveur web autonome
        """

        # Algorithme en 5 étapes
        # - Import de librairies capables de gérer des entrées et
        #   sortie sur une socket TCP AIFNET et des requêtes HTTP.
        # - Configuration du port et du nom de domaine sur lequel
        #   pourra être requeté le serveur Web
        # - Apport au serveur d'une fonction de gestion des requêtes
        # - Création du serveur
        # - Démarrage du serveur


        # Import des librairies nécessaires
        import http.server
        import socketserver


        # Configuration du port d'écoute du serveur et du nom de domaine de l'application
        port = 8080
        domain = "localhost"
        network_config = (domain, port)


        # Fabrication d'une fonction chargée de réagir aux requêtes
        handler = http.server.SimpleHTTPRequestHandler


        # Création du serveur
        httpd = socketserver.TCPServer(network_config, handler)


        # Affichage d'informations sur le terminal
        if True == self.debug:
            print("serving at http://%s:%d" % (domain, port))


        # Démarrage du serveur
        httpd.serve_forever()



    """
    * Specifications

    """

    class USER:
        """
        Spécification d'un utilisateur
        """
        Name = "";
        Email = "";
        Password = "";

    class ADMIN(USER):
        """
        Spécification d'un administrateur
        """
        pass

    class CAPTOR:
        """
        Spécification d'un capteur
        """
        pass

    class MOTOR:
        """
        Spécification d'un moteur
        """
        pass

    class CONTROLPAGE:
        """
        Specification d'une page de contrôle
        """
        pass

    class CONFIGPAGE:
        """
        Specification d'une page de configuration
        """
        pass

    class EXCEPTION:
        """
        Specification d'une page de configuration
        """
        pass

    class ERREUR:
        """
        Specification d'une page de configuration
        """
        pass


p = Robot()

if(p.debug == True):
    print(p.CODELENGTH())

p.HTTP()
